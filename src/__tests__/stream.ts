import { Extractor, StreamExpect } from '../stream_expect';
import {PassThrough} from 'stream';
import * as BbPromise from 'bluebird';

describe('StreamExpect', () => {
	beforeEach(() => {
		this.str = new PassThrough();
		this.se = new StreamExpect(this.str);
	});

	it('Expects data', async () => {
		this.str.push('foobar\n');
		let line = await this.se.expect('foobar');
		expect(line).toEqual('foobar');
		this.str.end();
	});

	it('Expects data on chunked input', async () => {
		this.str.push('foo');
		this.str.push('bar\n');
		let line = await this.se.expect('foobar');
		expect(line).toEqual('foobar');

		this.str.push('foo');
		this.str.push('bar');
		this.str.push('\n');
		line = await this.se.expect('foobar');
		expect(line).toEqual('foobar');

		this.str.end();
	});

	it('Fails on bad expected data', async () => {
		this.str.push('foobar\n');
		await expect(this.se.expect('not foobar')).rejects.toEqual(
			new Error("Bad line 'foobar' encountered while expecting 'not foobar'")
		);
		this.str.end();
	});

	it('Fails on bad expected data on chunked input', async () => {
		this.str.push('foo');
		this.str.push('bar\n');
		await expect(this.se.expect('not foobar')).rejects.toEqual(
			new Error("Bad line 'foobar' encountered while expecting 'not foobar'")
		);
		this.str.end();
	});

	it('Waits for data', async () => {
		this.str.push('foo\n');
		this.str.push('bar\n');
		this.str.push('baz\n');
		let line = await this.se.wait('baz');
		expect(line).toEqual('baz');
		this.str.end();
	});

	it('Waits for data on chunked input', async () => {
		this.str.push('fo');
		this.str.push('o\nbar\n');
		let line = await this.se.wait('bar');
		expect(line).toEqual('bar');
		this.str.end();
	});

	it('Fails on bad wait data', async () => {
		expect.assertions(1);

		this.str.push('foo\n');

		try {
			await this.se.wait('bar').timeout(100);
		} catch (e) {
			expect(e).toBeInstanceOf(BbPromise.TimeoutError);
		}
	});

	it('Fails on bad wait data past end of stream', async () => {
		expect.assertions(1);

		this.str.push('foo\n');
		this.str.end();

		try {
			await this.se.wait('bar');
		} catch (e) {
			expect(e).toEqual(new Error("End of stream encountered while waiting for 'bar' line"));
		}
	});

	it('Allows expect on unconsumed output after stream end', async () => {
		this.str.push('foobar\n');
		this.str.end();
		let line = await this.se.expect('foobar');
		expect(line).toEqual('foobar');
	});

	it('Allows wait on unconsumed output after stream end', async () => {
		this.str.push('foo\n');
		this.str.push('bar\n');
		this.str.end();
		let line = await this.se.wait('bar');
		expect(line).toEqual('bar');
	});

	it('Resolves expect on unconsumed output after stream end', async () => {
		this.str.push('foobar');
		this.str.end();
		let line = await this.se.expect('foobar');
		expect(line).toEqual('foobar');
	});

	it('Resolves wait on unconsumed output after stream end', async () => {
		this.str.push('foo\n');
		this.str.push('bar');
		this.str.end();
		let line = await this.se.wait('bar');
		expect(line).toEqual('bar');
	});

	it('Fails when expecting past the end of stream', async () => {
		this.str.push('foo\n');
		this.str.end();

		let line = await this.se.expect('foo');
		expect(line).toEqual('foo');

		await expect(this.se.expect('bar')).rejects.toEqual(
			new Error('Unable to expect for data once all output had been consumed')
		);

		await expect(this.se.wait('bar')).rejects.toEqual(
			new Error('Unable to wait for data once all output had been consumed')
		);
	});

	it('Expects via regexp', async () => {
		this.str.push('foobar\n');
		this.str.push('foobar\n');
		let line = await this.se.expect(/oba/);
		expect(line).toEqual('foobar');
		line = await this.se.expect(/bar$/);
		expect(line).toEqual('foobar');
		this.str.end();
	});

	it('Fails on bad expect via regexp', async () => {
		this.str.push('foobar\n');
		await expect(this.se.expect(/baz/)).rejects.toEqual(
			new Error("Bad line 'foobar' encountered while expecting '/baz/'")
		);
		this.str.end();
	});

	it('Waits via regexp', async () => {
		this.str.push('foo\n');
		this.str.push('bar\n');
		let line = await this.se.wait(/r$/);
		expect(line).toEqual('bar');
		this.str.end();
	});

	it('Fails on bad wait data via regex past end of stream', async () => {
		expect.assertions(1);

		this.str.push('foo\n');
		this.str.end();

		try {
			await this.se.wait(/bar/);
		} catch (e) {
			expect(e).toEqual(new Error("End of stream encountered while waiting for '/bar/' line"));
		}
	});

	it('Mixes wait and expect', async () => {
		this.str.push('this line is expected\n');
		this.str.push('this line is expected, too\n');
		this.str.push('this line is expected, via regexp\n');
		this.str.push('this line is skipped\n');
		this.str.push('this line is waited\n');
		this.str.push('this line is skipped, too\n');
		this.str.push('this line is skipped, too\n');
		this.str.push('this line is waited, via regexp\n');
		this.str.push('this line is expected again\n');

		expect(await this.se.expect('this line is expected')).toEqual('this line is expected');
		expect(await this.se.expect('this line is expected, too')).toEqual('this line is expected, too');
		expect(await this.se.expect(/this.*expected.*regexp/)).toEqual('this line is expected, via regexp');
		expect(await this.se.wait('this line is waited')).toEqual('this line is waited');
		expect(await this.se.wait(/this.*waited.*regexp/)).toEqual('this line is waited, via regexp');
		expect(await this.se.expect('this line is expected again')).toEqual('this line is expected again');

		this.str.end();
	});

	it('Expects correctly when data is first', async () => {
		this.str.push('foobar\n');
		let line = await this.se.expect('foobar');
		expect(line).toEqual('foobar');
		this.str.end();

	});

	it('Expects correctly when expectation is first', async () => {
		let line = this.se.expect('foobar');
		this.str.push('foobar\n');
		expect(await line).toEqual('foobar');
		this.str.end();

	});

	it('Waits correctly when data is first', async () => {
		this.str.push('foo\n');
		this.str.push('bar\n');
		this.str.push('baz\n');
		let line = await this.se.wait('baz');
		expect(line).toEqual('baz');
		this.str.end();

	});

	it('Waits correctly when expectation is first', async () => {
		let line = this.se.wait('baz');
		this.str.push('foo\n');
		this.str.push('bar\n');
		this.str.push('baz\n');
		expect(await line).toEqual('baz');
		this.str.end();
	});

	it('Waits with not expected - not expected not appearing', async () => {
		this.str.push('foo\n');
		this.str.push('foo1\n');
		this.str.push('foo2\n');
		this.str.push('bar\n');
		let line = await this.se.wait(/bar/, /foo99/);
		expect(line).toEqual('bar');
		this.str.end();
	});

	it('Waits with not expected - not expected appeared', async () => {
		this.str.push('foo\n');
		this.str.push('foo1\n');
		this.str.push('foo2\n');
		this.str.push('bar\n');
		expect.assertions(1);
		try {
			await this.se.wait( /bar/, /foo2/ );
		} catch (err) {
			expect(err.message).toMatch("Not expected data appeared");
		}
		this.str.end();
	});

	it( 'Extract single value from single line', async () => {
		this.str.push('foo 123\n');
		let testNum:any[] = [];
		await this.se.wait( new Extractor(/foo (\d+)/, testNum ) );
		expect(testNum[0]).toEqual('123');
		this.str.end();
	});

	it( 'Extract single value within lines', async () => {
		this.str.push('foo\n');
		this.str.push('foo321\n');
		this.str.push('foo 123\n');
		let testNum:any[] = [];
		await this.se.wait( new Extractor(/foo(\d+)/, testNum ) );
		expect(testNum[0]).toEqual('321');
		this.str.end();
	});

	it( 'Extract multiple values from single line', async () => {
		this.str.push('foo 1 2 3 4 5\n');
		let testNum:any[] = [];
		await this.se.wait( new Extractor(/foo (\d) (\d) (\d) (\d) (\d)/, testNum ) );
		expect(testNum.length).toEqual(5);
		expect(testNum[0]).toEqual('1');
		expect(testNum[4]).toEqual('5');
		this.str.end();
	});
});
