import * as BbPromise from 'bluebird';
import {Spawner} from '../index';

describe('Spawner', () => {
	beforeEach(async () => {
		this.sp = new Spawner();

		this.process = this.sp.run('bc');
	});

	it('Spawns', async () => {
		this.sp.sendLine('2*3');
		expect(await this.sp.stdout.expect('6')).toEqual('6');
		this.sp.sendEof();
		let res = await this.process;
		expect(res).toMatchObject({exitCode: 0, signal: null });
		expect( await res.stdout() ).toEqual(["6"]);
		expect( await res.stderr() ).toEqual([]);
	});

	it('Spawns and buffers', async () => {
		this.sp.sendLine('2*3');
		this.sp.sendEof();

		await BbPromise.delay(100);
		expect(await this.sp.stdout.expect('6')).toEqual('6');
		let res = await this.process;
		expect(res).toMatchObject({exitCode: 0, signal: null });
		expect( await res.stdout() ).toEqual(["6"]);
		expect( await res.stderr() ).toEqual([]);
	});
});
