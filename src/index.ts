/// <reference types="node" />

import * as BbPromise from 'bluebird';
import * as _ from 'lodash';
import {spawn, ChildProcess, SpawnOptions} from 'child_process';

import {StreamExpect} from './stream_expect';
export {StreamExpect, Expectation, ExpectedData, Extractor} from './stream_expect';

export type ExecutionResult = {
	exitCode: number;
	signal: string;
	stdout: ()=>Promise<string[]>;
	stderr: ()=>Promise<string[]>;
};

export class Spawner {
	public process: ChildProcess = undefined;
	public stdout: StreamExpect = undefined;
	public stderr: StreamExpect = undefined;

	public run = (cmd: string, args?: string[], opts?: SpawnOptions): BbPromise<ExecutionResult> => {
		return new BbPromise((resolve, reject) => {
			let finished = false;

			this.process = spawn(cmd, args, opts);

			if (this.process.stdout) {
				this.stdout = new StreamExpect(this.process.stdout);
			}

			if (this.process.stderr) {
				this.stderr = new StreamExpect(this.process.stderr);
			}

			this.process.on('exit', async (exitCode: number, signal: string): Promise<void> => {
				if (!finished) {
					finished = true;
				}

				let res: ExecutionResult = {exitCode, signal, stdout: async ()=> [], stderr: async ()=> [] };

				if( this.stdout ) {
					res.stdout = async () => await this.stdout.finish();
				}

				if( this.stderr ) {
					res.stderr = async () => await this.stderr.finish();
				}

				resolve( res );
			});

			this.process.on('error', (err: Error): void => {
				if (!finished) {
					finished = true;
				}

				reject(err);
			});
		});
	};

	public sendLine = (line: string): void => {
		if (!_.isNil(_.get(this, 'process.stdin'))) {
			this.process.stdin.write(line + '\n');
		} else {
			throw new Error('Child process stdin is not opened');
		}
	};

	public sendEof = (): void => {
		if (!_.isNil(_.get(this, 'process.stdin'))) {
			this.process.stdin.destroy();
		} else {
			throw new Error('Child process stdin is not opened');
		}
	};
}
