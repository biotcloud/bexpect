/// <reference types="node" />

import * as _ from 'lodash';
import * as BbPromise from 'bluebird';

export type Expectation = (line: string) => boolean;

export class Extractor {
	private format: RegExp;
	private vars: any[];

	constructor(format:RegExp, vars: any[]) {
		this.format = format;
		this.vars = vars;
	}

	public get(str: string): boolean {
		var matched = str.match(this.format);
		if(matched == null) {
			return false;
		}
		matched.forEach((value, index) => {
			if (index < 1) return;
			this.vars.push(value);
		});

		return true;
	}
}

export type ExpectedData = string | RegExp | Extractor;

export class StreamExpect {
	private stream: NodeJS.ReadableStream = undefined;
	private output: string = '';
	private lines: string[] = [];
	private expectations: Expectation[] = [];
	private finished: BbPromise<string[]> = undefined;
	private resolveFinished:Function = undefined;
	private rejectFinished:Function = undefined;
	public processedLines: string[] = [];

	constructor(str: NodeJS.ReadableStream) {
		this.finished = new BbPromise( (resolve, reject)=>{
			this.resolveFinished = resolve;
			this.rejectFinished = reject;
		});

		this.stream = str;

		this.registerListeners();
	}

	private onData = (chunk: Buffer) => {
		this.output += chunk.toString();

		this.scanLines();
	};

	private scanLines = (): void => {
		let idx: number = undefined;
		let gotLines: boolean = false;

		do {
			idx = _.indexOf(this.output, '\n');

			if (idx >= 0) {
				if (idx > 0) {
					let line: string = this.output.substr(0, idx);

					if (line[line.length - 1] == '\r') {
						line = line.substr(0, line.length - 1);
					}

					if (line.length > 0) {
						this.lines.push(line);
						gotLines = true;
					}
				}

				this.output = this.output.substr(idx + 1);
			}
		} while (idx >= 0);

		if (gotLines == true) {
			this.processLines();
		}
	};

	private processLines = () => {
		// console.log(`PL`, this.lines, this.expectations);
		while ((this.lines.length > 0) && (this.expectations.length > 0)) {
			let line: string = this.lines.shift();
			let expectation: Expectation = this.expectations[0];

			let finished = expectation(line);

			if (finished == true) {
				this.expectations.shift();
			}

			if( _.isNil( line ) ) {
				this.resolveFinished( this.processedLines );
			} else {
				this.processedLines.push( line );
			}
		}

		if( (this.lines.length==1) && (_.isNil(this.lines[0])) && (this.expectations.length == 0 ) ) {
			this.lines.shift();
			this.resolveFinished( this.processedLines );
		}
	};

	private onEnd = () => {
		if (this.output.length > 0) {
			this.lines.push(this.output);
			this.output = '';
		}

		this.lines.push(undefined);
		this.processLines();

		this.unregisterListeners();
	};

	public finish = ():BbPromise<string[]> => {
		if( (this.lines.length > 0) && (this.finished.isFulfilled() == false) ) {
			let allLines = this.processedLines.concat( this.lines );
			allLines.pop(); // remove 'undefined' marker from the end

			this.resolveFinished( allLines );
		}

		return this.finished;
	};

	private onError = (e: Error) => {
		console.log('onerror', e);
	};

	private onClose = () => {
		console.log('onclose');
	};

	private registerListeners() {
		this.stream.on('data', this.onData);
		this.stream.on('end', this.onEnd);
		this.stream.on('error', this.onError);
		this.stream.on('close', this.onClose);
	}

	private unregisterListeners() {
		this.stream.removeListener('data', this.onData);
		this.stream.removeListener('end', this.onEnd);
		this.stream.removeListener('error', this.onError);
		this.stream.removeListener('close', this.onClose);
	}

	public expect = (expData: ExpectedData): BbPromise<string> => {
		if( this.finished.isFulfilled() == true ) {
			return BbPromise.reject( new Error( 'Unable to expect for data once all output had been consumed' ) );
		}

		return new BbPromise((resolve: Function, reject: Function) => {
			let expectation: Expectation = (actLine: string): boolean => {
				if (_.isNil(actLine)) {
					reject( new Error( `End of stream encountered while expecting '${expData}' line` ) );
				} else {
					if( _.isString( expData ) ) {
						if( expData === actLine ) {
							resolve( actLine );
						} else {
							reject( new Error( `Bad line '${actLine}' encountered while expecting '${expData}'` ) );
						}
					} else if( _.isRegExp( expData ) ) {
						if( expData.test( actLine ) == true ) {
							resolve( actLine );
						} else {
							reject( new Error( `Bad line '${actLine}' encountered while expecting '${expData}'` ) );
						}
					} else if( expData instanceof Extractor) {
						reject( new Error( `Cannot extract '${expData}' from '${actLine}'`) );
						if( expData.get( actLine ) == true ) {
							resolve( actLine );
						} else {
							reject( new Error( `Cannot extract '${expData}' from '${actLine}'`) );
						}
					} else {
						reject( new Error( 'Invalid parameters' ) );
					}
				}

				return true;
			};

			this.expectations.push(expectation);

			this.processLines();
		});
	};

	private lineMatches(expData: ExpectedData, line: string): boolean {
		if (expData === line) {
			return true;
		} else {
			if (_.isRegExp(expData)) {
				if (expData.test(line) == true) {
					return true;
				}
			} else if( expData instanceof Extractor) {
				return expData.get( line );
			}
			return false;
		}
	}

	public wait = (expData: ExpectedData, notExpData?: ExpectedData): BbPromise<string> => {
		if( this.finished.isFulfilled() == true ) {
			return BbPromise.reject( new Error( 'Unable to wait for data once all output had been consumed' ) );
		}

		return new BbPromise((resolve: Function, reject: Function) => {
			let expectation: Expectation = (actLine: string): boolean => {
				if (_.isNil(actLine)) {
					reject(new Error(`End of stream encountered while waiting for '${expData}' line`));
				} else {
					if (this.lineMatches(notExpData, actLine)) {
						reject(new Error(`Not expected data appeared: ${notExpData}`));
					} else if (this.lineMatches(expData, actLine)) {
						resolve(actLine);
						return true;
					}
				}
				return false;
			};

			this.expectations.push(expectation);

			this.processLines();
		});
	};
}
