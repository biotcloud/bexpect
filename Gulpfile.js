var gulp = require( 'gulp' );
var babel = require( 'gulp-babel' );
var plumber = require( 'gulp-plumber' );
var ts = require('gulp-typescript');
var tsProject = ts.createProject("tsconfig.json");

gulp.task("dist", [], function () {
	var tsResult = tsProject.src()
		.pipe( plumber() )
		.pipe(tsProject());

	tsResult.js.pipe( babel( {} ) )
		.pipe(gulp.dest("dist"));

	return tsResult.dts
		.pipe( gulp.dest( 'dist' ) );
});

gulp.task( 'watch', [ 'default' ], function() {
	return gulp.watch( [ '{src, config}/**/*.{ts, js}' ], [ 'dist' ] );
} );

gulp.task( 'default', [ 'dist' ] );
