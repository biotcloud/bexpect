/// <reference types="node" />
import * as BbPromise from 'bluebird';
export declare type Expectation = (line: string) => boolean;
export declare class Extractor {
    private format;
    private vars;
    constructor(format: RegExp, vars: any[]);
    get(str: string): boolean;
}
export declare type ExpectedData = string | RegExp | Extractor;
export declare class StreamExpect {
    private stream;
    private output;
    private lines;
    private expectations;
    private finished;
    private resolveFinished;
    private rejectFinished;
    processedLines: string[];
    constructor(str: NodeJS.ReadableStream);
    private onData;
    private scanLines;
    private processLines;
    private onEnd;
    finish: () => BbPromise<string[]>;
    private onError;
    private onClose;
    private registerListeners();
    private unregisterListeners();
    expect: (expData: ExpectedData) => BbPromise<string>;
    private lineMatches(expData, line);
    wait: (expData: ExpectedData, notExpData?: ExpectedData) => BbPromise<string>;
}
