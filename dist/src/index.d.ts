/// <reference types="node" />
import * as BbPromise from 'bluebird';
import { ChildProcess, SpawnOptions } from 'child_process';
import { StreamExpect } from './stream_expect';
export { StreamExpect, Expectation, ExpectedData, Extractor } from './stream_expect';
export declare type ExecutionResult = {
    exitCode: number;
    signal: string;
    stdout: () => Promise<string[]>;
    stderr: () => Promise<string[]>;
};
export declare class Spawner {
    process: ChildProcess;
    stdout: StreamExpect;
    stderr: StreamExpect;
    run: (cmd: string, args?: string[], opts?: SpawnOptions) => BbPromise<ExecutionResult>;
    sendLine: (line: string) => void;
    sendEof: () => void;
}
