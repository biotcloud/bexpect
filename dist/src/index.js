"use strict";
/// <reference types="node" />

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
            try {
                step(generator.next(value));
            } catch (e) {
                reject(e);
            }
        }
        function rejected(value) {
            try {
                step(generator["throw"](value));
            } catch (e) {
                reject(e);
            }
        }
        function step(result) {
            result.done ? resolve(result.value) : new P(function (resolve) {
                resolve(result.value);
            }).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const BbPromise = require("bluebird");
const _ = require("lodash");
const child_process_1 = require("child_process");
const stream_expect_1 = require("./stream_expect");
var stream_expect_2 = require("./stream_expect");
exports.StreamExpect = stream_expect_2.StreamExpect;
exports.Extractor = stream_expect_2.Extractor;
class Spawner {
    constructor() {
        this.process = undefined;
        this.stdout = undefined;
        this.stderr = undefined;
        this.run = (cmd, args, opts) => {
            return new BbPromise((resolve, reject) => {
                let finished = false;
                this.process = child_process_1.spawn(cmd, args, opts);
                if (this.process.stdout) {
                    this.stdout = new stream_expect_1.StreamExpect(this.process.stdout);
                }
                if (this.process.stderr) {
                    this.stderr = new stream_expect_1.StreamExpect(this.process.stderr);
                }
                this.process.on('exit', (exitCode, signal) => __awaiter(this, void 0, void 0, function* () {
                    if (!finished) {
                        finished = true;
                    }
                    let res = { exitCode, signal, stdout: () => __awaiter(this, void 0, void 0, function* () {
                            return [];
                        }), stderr: () => __awaiter(this, void 0, void 0, function* () {
                            return [];
                        }) };
                    if (this.stdout) {
                        res.stdout = () => __awaiter(this, void 0, void 0, function* () {
                            return yield this.stdout.finish();
                        });
                    }
                    if (this.stderr) {
                        res.stderr = () => __awaiter(this, void 0, void 0, function* () {
                            return yield this.stderr.finish();
                        });
                    }
                    resolve(res);
                }));
                this.process.on('error', err => {
                    if (!finished) {
                        finished = true;
                    }
                    reject(err);
                });
            });
        };
        this.sendLine = line => {
            if (!_.isNil(_.get(this, 'process.stdin'))) {
                this.process.stdin.write(line + '\n');
            } else {
                throw new Error('Child process stdin is not opened');
            }
        };
        this.sendEof = () => {
            if (!_.isNil(_.get(this, 'process.stdin'))) {
                this.process.stdin.destroy();
            } else {
                throw new Error('Child process stdin is not opened');
            }
        };
    }
}
exports.Spawner = Spawner;