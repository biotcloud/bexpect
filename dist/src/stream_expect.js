"use strict";
/// <reference types="node" />

Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
const BbPromise = require("bluebird");
class Extractor {
    constructor(format, vars) {
        this.format = format;
        this.vars = vars;
    }
    get(str) {
        var matched = str.match(this.format);
        if (matched == null) {
            return false;
        }
        matched.forEach((value, index) => {
            if (index < 1) return;
            this.vars.push(value);
        });
        return true;
    }
}
exports.Extractor = Extractor;
class StreamExpect {
    constructor(str) {
        this.stream = undefined;
        this.output = '';
        this.lines = [];
        this.expectations = [];
        this.finished = undefined;
        this.resolveFinished = undefined;
        this.rejectFinished = undefined;
        this.processedLines = [];
        this.onData = chunk => {
            this.output += chunk.toString();
            this.scanLines();
        };
        this.scanLines = () => {
            let idx = undefined;
            let gotLines = false;
            do {
                idx = _.indexOf(this.output, '\n');
                if (idx >= 0) {
                    if (idx > 0) {
                        let line = this.output.substr(0, idx);
                        if (line[line.length - 1] == '\r') {
                            line = line.substr(0, line.length - 1);
                        }
                        if (line.length > 0) {
                            this.lines.push(line);
                            gotLines = true;
                        }
                    }
                    this.output = this.output.substr(idx + 1);
                }
            } while (idx >= 0);
            if (gotLines == true) {
                this.processLines();
            }
        };
        this.processLines = () => {
            // console.log(`PL`, this.lines, this.expectations);
            while (this.lines.length > 0 && this.expectations.length > 0) {
                let line = this.lines.shift();
                let expectation = this.expectations[0];
                let finished = expectation(line);
                if (finished == true) {
                    this.expectations.shift();
                }
                if (_.isNil(line)) {
                    this.resolveFinished(this.processedLines);
                } else {
                    this.processedLines.push(line);
                }
            }
            if (this.lines.length == 1 && _.isNil(this.lines[0]) && this.expectations.length == 0) {
                this.lines.shift();
                this.resolveFinished(this.processedLines);
            }
        };
        this.onEnd = () => {
            if (this.output.length > 0) {
                this.lines.push(this.output);
                this.output = '';
            }
            this.lines.push(undefined);
            this.processLines();
            this.unregisterListeners();
        };
        this.finish = () => {
            if (this.lines.length > 0 && this.finished.isFulfilled() == false) {
                let allLines = this.processedLines.concat(this.lines);
                allLines.pop(); // remove 'undefined' marker from the end
                this.resolveFinished(allLines);
            }
            return this.finished;
        };
        this.onError = e => {
            console.log('onerror', e);
        };
        this.onClose = () => {
            console.log('onclose');
        };
        this.expect = expData => {
            if (this.finished.isFulfilled() == true) {
                return BbPromise.reject(new Error('Unable to expect for data once all output had been consumed'));
            }
            return new BbPromise((resolve, reject) => {
                let expectation = actLine => {
                    if (_.isNil(actLine)) {
                        reject(new Error(`End of stream encountered while expecting '${expData}' line`));
                    } else {
                        if (_.isString(expData)) {
                            if (expData === actLine) {
                                resolve(actLine);
                            } else {
                                reject(new Error(`Bad line '${actLine}' encountered while expecting '${expData}'`));
                            }
                        } else if (_.isRegExp(expData)) {
                            if (expData.test(actLine) == true) {
                                resolve(actLine);
                            } else {
                                reject(new Error(`Bad line '${actLine}' encountered while expecting '${expData}'`));
                            }
                        } else if (expData instanceof Extractor) {
                            reject(new Error(`Cannot extract '${expData}' from '${actLine}'`));
                            if (expData.get(actLine) == true) {
                                resolve(actLine);
                            } else {
                                reject(new Error(`Cannot extract '${expData}' from '${actLine}'`));
                            }
                        } else {
                            reject(new Error('Invalid parameters'));
                        }
                    }
                    return true;
                };
                this.expectations.push(expectation);
                this.processLines();
            });
        };
        this.wait = (expData, notExpData) => {
            if (this.finished.isFulfilled() == true) {
                return BbPromise.reject(new Error('Unable to wait for data once all output had been consumed'));
            }
            return new BbPromise((resolve, reject) => {
                let expectation = actLine => {
                    if (_.isNil(actLine)) {
                        reject(new Error(`End of stream encountered while waiting for '${expData}' line`));
                    } else {
                        if (this.lineMatches(notExpData, actLine)) {
                            reject(new Error(`Not expected data appeared: ${notExpData}`));
                        } else if (this.lineMatches(expData, actLine)) {
                            resolve(actLine);
                            return true;
                        }
                    }
                    return false;
                };
                this.expectations.push(expectation);
                this.processLines();
            });
        };
        this.finished = new BbPromise((resolve, reject) => {
            this.resolveFinished = resolve;
            this.rejectFinished = reject;
        });
        this.stream = str;
        this.registerListeners();
    }
    registerListeners() {
        this.stream.on('data', this.onData);
        this.stream.on('end', this.onEnd);
        this.stream.on('error', this.onError);
        this.stream.on('close', this.onClose);
    }
    unregisterListeners() {
        this.stream.removeListener('data', this.onData);
        this.stream.removeListener('end', this.onEnd);
        this.stream.removeListener('error', this.onError);
        this.stream.removeListener('close', this.onClose);
    }
    lineMatches(expData, line) {
        if (expData === line) {
            return true;
        } else {
            if (_.isRegExp(expData)) {
                if (expData.test(line) == true) {
                    return true;
                }
            } else if (expData instanceof Extractor) {
                return expData.get(line);
            }
            return false;
        }
    }
}
exports.StreamExpect = StreamExpect;