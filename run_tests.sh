#!/bin/bash
cd -P "$( dirname "${BASH_SOURCE[0]}" )"

yarn test -- --runInBand --watch
